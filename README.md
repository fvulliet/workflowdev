# WorkflowDev

## exemples des scripts à utiliser pour Q2APP

### tests unitaires

```
# depuis le répertoire de build
# pour générer l'exécutable de tests unitaires
cmake ../ -DTESTS=ON -DCOVERAGE=ON -DCMAKE_PREFIX_PATH=/home/ca/Qt5.4.2/5.4/gcc_64/ && make test_all -j8

# lancer l'éxécutable
./test_all

# lancer les tests unitaires avec affichage des statistiques seulement
make coverage
# lancer les tests unitaires avec affichage des statistiques et détail du coverage dans le browser
make coverage_show
```

### analyse statique de code 

```
# depuis le répertoire de build
# tout le code
make cppcheck
# tout le code; affichage du rapport dans le browser
make cppcheck_show 
# uniquement le code en lien avec q2monitor
make cppcheck_q2monitor
# uniquement le code en lien avec q2monitor; affichage du rapport dans le browser
make cppcheck_show_q2monitor
```

### duplication de code 

```
# depuis /work_disk/Q2APP
sh ./runStaticAnalyzis.sh
```