---
*Un workflow c'est quoi ?*

---
C'est un ensemble d'outils, de méthodes, et de bonnes pratiques, pour emmener son travail d'un statut **EN COURS** vers un statut **FAIT**.

---

*Bien, mais pourquoi mettre en place et suivre un WF ?*

---
- Pour faire travailler les équipes de manière rapide, fiable, et partagée => **efficace** ! 
- Pour **partager** un référentiel commun avec la communauté des développeurs 
	- trouver plus facilement du support (internet, consultants, collègues..)
	- attirer et intégrer plus facilement les nouvelles recrues (et les conserver aussi ?)
- Pour donner plus de **sens** à notre travail et augmenter notre **valeur ajoutée**

---
*Super, mais à quel prix ?*

---
La plupart des outils sont gratuits! <br>
Mais cela coûte un peu de **temps** pour monter en compétences et appréhender les outils. <br>
Et sans doute quelques efforts du côté de la **conception** du logiciel, pour pousser au maximum la modularité, le découpage, les abstractions.. <br>
Et peut-être un peu de **'politique'**, car c'est une démarche **collective** qui peut impacter plusieurs métiers..