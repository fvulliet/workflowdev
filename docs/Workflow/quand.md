# Quand devrait-on commencer à parler de workflow et de méthodologie

**TOUJOURS** ! Car l'amélioration doit faire partie de notre manière de travailler.

![amélioration continue](../../img/amelioration-continue.png "amélioration continue")

Il est déjà trop tard...

- quand ça devient difficile de faire des **changements** dans le code 
- quand les bases de codes sont trop **complexes** et illisibles (<=> non maintenable)
- quand la **courbe d'apprentissage** des nouveaux est trop longue, ou que ça les décourage de rester !

![mur](../../img/face au mur.jpg "mur")

---
> *Tiens, vous avez déjà été confrontés à ces difficultés ?* 

---

Alors peut-être que vous avez aussi été confrontés à ce genre de réflexions:

- ***"on n'a pas de temps pour ça"*** <br>
Ah bon ? Est-ce qu'on aura plus de temps quand il faudra revenir sur ce projet si des bugs sont remontés ? 
- ***"ce n'est pas pour nous, on est dans l'embarqué donc c'est compliqué"*** <br>
Plus un système est complexe, plus ce sera difficile à débugger tardivement... 
- ***"pas besoin, je ne fais pas de bugs"*** <br>
Soit, mais quid des changements d'environnement de travail dans le futur et des effets de bord induits ? 
- ***"c'est une perte de temps / je teste tout à la main / je suis seul sur mon projet"*** <br>
A l'échelle d'une entreprise, la maintenance, la fiabilité, dans le temps sont capitaux ! et la "transférabilité" aussi..

![too busy to improve](../../img/TooBusyToImprove.png "too busy to improve ?")

## Importance de la **veille technologique**
Il faut être capable de relever la tête de temps en temps. <br>
Il est de notre **responsabilité** de façonner et de consolider notre workflow. <br>
Car notre job n'est pas de "pisser" du code, mais de développer des systèmes **robustes et pérennes** ! 

---
> *C'est ce qui donne de l'intérêt et de la valeur à notre travail*

---