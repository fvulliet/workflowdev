# Pourquoi est-ce que c'est important pour les développeurs software

La maintenabilité, la robustesse, la qualité de nos délivrables doivent être notre leitmotiv.

---
**"On ne fait pas un code qui marche aujourd'hui, mais un code qui marchera demain !"**.

---

![pérennité](../../img/Gerantdesarl_2019-05-06_perennite_sarl_sas.jpg "pérennité")

On travaille souvent en équipe, et l'efficacité de l'équipe est importante.

*Il est préférable de passer du temps aujourd'hui à essayer d'en gagner pour demain*

---
> Il faut donc éviter de multiplier les taches répétitives ! <br>

---
Il faut que le workflow (et le code) reste simple (KISS), pour ne laisser personne sur la touche. A nous de le **façonner** à notre convenance.

![kiss](../../img/Kiss.jpg "kiss")

## WF pour augmenter la qualité du code 
Le workflow permet d'atteindre une meilleure qualité, grâce à des outils tels que **l'analyse statique de code, les tests unitaires**.

La qualité est renforcée par une documentation pertinente, des revues de code, et idéalement une approche Test Driven Development.

## WF pour garantir la maintenabilité 
Le workflow permet d'atteindre une meilleure maintenabilité, via **le style, le lint** et grâce à la **modularité** (encourage le découpage en composants réutilisables), et l'intégration continue (**CI**).
 
## WF pour améliorer la qualité des livrables 
Le workflow permet d'atteindre une meilleure qualité du produit final, grâce à plus de **tests** (unitaires, intégration, fonctionnels).

## WF pour relacher la pression
Le workflow permet de se libérer des angoisses dès lors qu'il s'agit de générer/déployer une application, grâce au **déploiement continu (CD)**.
  
Les machines travaillent sans rechigner, sans stress, sans états d'âme !

Donc plus on les sollicite, plus on a de temps à consacrer aux réflexions "métier" intéressantes, que seules nous pouvons maîtriser => notre **valeur ajoutée** est majorée !

![robot](../../img/robotNuit.jpg "robot")