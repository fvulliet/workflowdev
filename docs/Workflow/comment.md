# Comment l'implémenter

A nous de forger notre propre solution !

Quelques exemples possibles à notre niveau:

- avoir des projets et des dépôts **homogènes** (gitlab, règles de CI et d'utilisation des outils, environnements de build conteneurisés, organisation des répertoirees, nommages, style, règles de codage)
- utiliser des technologies et des frameworks **communs**, et documentés (READMEs, guidelines)
- mettre en place une **gestion de projet Agile** : backlog & sprints, kanban pour l'avancement des tickets, daily scrum..
- définir des **référents** par composants pour donner du sens et responsabiliser les individus
- spikes et **brainstorming** pour évaluer les possibilités et structurer les phases de conception
- travail en **équipe** : pair programming, revues de code, merge request, s'appuyer sur les résultats de la CI pour progresser
- **qualité et revues** : revue de code hebdomadaire pour montrer aux autres et créer des échanges, weekly builds pour valider en permanence en partant de zéro, pair coding et revues (car ça permet d'apprendre en permanence)

---
>**Une bonne semaine est une semaine où on a appris quelque chose, où on est fier d'avoir partagé son travail.**

---