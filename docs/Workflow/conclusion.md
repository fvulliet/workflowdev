# Conclusion

Mettre en place et maintenir un workflow, écrire des tests.. ça peut paraître écrasant..

- mais on peut très bien **démarrer doucement et atteindre un rythme rapide**, plutôt que de faire le contraire
- combien d'équipes sont **polluées** par la maintenance et les taches répétitives ?
- les TU représentent environ 50% du temps de développement, mais ça **économise** davantage sur la maintenance et les résolutions de problèmes !
- la **dette technique** et le coût des bugs sont durs à mesurer, mais il faut toujours chercher à le minimiser !

![investir](../../../img/investir-nvx-marches.jpg "investir")

## Identifer des outils et **façonner** un workflow à sa mesure. 

On peut s'appuyer sur des méthodologies et les adapter ou les mélanger selon notre expérience et nos projets. <br>
Même si on est seul à développer, ça va **sécuriser** notre travail et nous faciliter la vie. <br>
Sécuriser son chemin de production est capital. <br>
Il n'y a pas de contexte où ça ne pourrait pas s'appliquer ! <br>
(Presque) tout le monde peut écrire du code, mais le déployer et le maintenir à large échelle est un art !<br>