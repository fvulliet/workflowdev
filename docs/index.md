![CA](../../img/chauvin.png "logo CA")

# WORKFLOW de développement logiciel (2024)

*Ce document est basé sur la présentation de Sylvain Fargier (CERN) en décembre 2023.*

Il présente en premier lieu ce qu'est un workflow de développement logiciel, quels sont les bénéfices qu'il est susceptible d'apporter, et donne des suggestions quant à sa mise en oeuvre. <br>

Un chapitre est ensuite consacré aux méthodes Agiles; pourquoi y recourir ? qu'est-ce qu'elles apportent ? et quelles contraintes introduisent-elles ? <br>

Puis, une sensibilisation à la conteneurisation des process est proposée, notamment autour de Docker.

Enfin, quelques notions sur la CICD sont abordées.

		
        
