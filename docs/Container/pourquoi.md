# Pourquoi y recourir ?

D'un point de vue organisation et travail en équipe, la conteneurisation permet de **partager les mêmes outils pour développer/builder une application** (toolchain, outils de test, outils de lint..)

- entre les développeurs 
- dans les pipelines CICD 

Partager les mêmes conteneurs, c'est garantir un certain **déterminisme**: ce qui est buildé chez un développeur est rigoureusement identique à ce qui est buildé chez un autre, ou sur un serveur de build.

---
> *"je peux partager mon environnement de développement avec mes collègues, et si je change de machine je peux retrouver en quelques commandes un environnement connu; je peux également utiliser cet environnement dans mes pipelines"*

---

![determinism](../../img/determinism.png "determinism")

Les images (= "templates" des conteneurs) existent sous forme de code (**"infrastructure as code"**) => versionnable, archivable à faible coût (peu de place utilisée), maintenable, partageable, facilement évolutif.

## Quelques bénéfices

- **configuration et livraison pertinente des applications**: la conteneurisation offre une manière rapide, standardisée, et efficiente (vis à vis des resources engagées) de développer, packager et exécuter les applications.
Les applications peuvent être distribuées sur des plateformes variées sans se soucier du framework, des librairies, et des problèmes de compatibilité
- **vitesse**: les conteneurs sont plus rapides que les VMs, ils sont également plus légers et utilisent peu de mémoire (on fait donc des économies au niveau IT)
- **portabilité**: les applications construites dans des conteneurs sont très portables, car elles sont vues comme des éléments unitaires
- **scalabilité**: docker peut être déployé sur plusieurs serveurs physiques, data servers, et plateformes cloud..
- **flexibilité du partage de resources**: plusieurs applications conteneurisées peuvent tourner sur une même machine et intéragir, même si elles sont isolées
- **pipelines**: Docker permet de standardiser le cycle de développement et la génération des releases
- **expérimentation**: expérimenter facilement et à moindre coût de nouvelles technologies: tout ce qu'on a à faire est de créer une image/container et de la/le supprimer quand on a terminé l'expérimentation !

---
> *La conteneurisation favorise l'optimisation du process de développement soft, en offrant un/des environnement/s duplicable/s et reproductible/s*

---

