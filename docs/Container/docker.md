# C'est quoi Docker ?

![docker](../../img/logo_docker.jpg "docker")

Il s'agit de l'application de virtualisation open source de référence, elle permet la manipulation des containers. <br> 

## Comment marche Docker ?

C'est à la fois assez proche et assez éloigné des machines virtuelles (VMs).
Les VMs utilisent des hyperviseurs pour partager les ressources et virtualiser les composants hardware. Conséquence : elles utilisent les ressources de façon non optimale.

Mais Docker virtualise l'OS, les containeurs tournent sur le même noyau et partagent les ressources de l'OS parmi tous les process Docker. <br>
Donc, les containers Docker sont légers, ils ne portent que les process essentiels de l'OS et les dépendances nécessaires, contrairement aux VMs. 

![davidGoliath](../../img/david-and-goliath.jpg "David and Goliath")

Docker utilise un modèle client-serveur pour créer, lancer, et distribuer les containers. Un serveur centralisé (dans ce cas, le docker daemon) fournit les services dont le client Docker a besoin. 

### 2 composants 

- le **Docker Engine**, qui est le coeur de Docker, il comprend
	- **dameon** (dockerd), le service qui tourne sur la machine hôte
	- **CLI** (Docker client), la ligne de commande Docker 
Le démon et la CLI communiquent via une interface réseau ou des sockets UNIX en utilisant l'API REST de Docker
- **Docker hub**, qui est un cloud pour héberger et distribuer les images Docker 


## Terminologie

- **Dockerfile**: ce fichier text comporte les commandes et instructions utilisées par le Docker Engine pour fabriquer une image. Les commandes sont standardisées (exemple: "docker build")
- **Docker Image**: c'est le template (read-only) qui fournit les spécifications requises pour configurer un environnement conteneurisée opérationnel. 
Les images conistent en plusieurs couches, chacune étant bâtie sur la précédente. La 1e couche (image parent) constitue le commencement de la plupart des Dockerfiles, et le fondement de l'image.
- **Docker Containers**: a container est un package standardisé qui contient tout ce dont une application a besoin pour s'exécuter. En somme, c'est l'instance active issue d'une image. 
Ce package exécutable inclut l'OS, le code, le runtime, les librairies, les fichiers de configuration, et toutes les dépendances utiles. <br>
- **Docker Engine**: c'est le coeur de Docker. Il inclut le dameon (dockerd), les APIs, et le Docker client.
- **Docker Daemon**: c'est le service responsable de l'exécution des instructions envoyées à Docker.
- **Docker Client**: c'est l'interface qui permet aux utilisateurs d'intéragir avec le daemon en utilisant l'API Docker. En somme, il reçoit les taches Docker (créer une image..), et les envoie vers les composants appropriés.
- **Docker Registry**: c'est un service pour héberger, stocker, et distribuer les images. Il peut héberger des images publiques ou privées, sur le cloud ou on-premise. 
- **Docker Hub**: c'est le dépôt officiel (cloud) pour les images Docker.
- **Docker Clients & Servers**: Docker dispose d'une architecture client-server. The Docker Daemon(Server) consists of all containers. The Docker Daemon(Server) receives the request from the Docker client through CLI or REST APIs and thus processes the request accordingly. Docker client and Daemon can be present on the same host or different host. 