# Exemple

- création d'un **Dockerfile** qui décrit l'image Docker que l'on souhaite construire

```
FROM debian:bullseye-slim

WORKDIR /root
ADD script.qs .

# Install some dev deps and tools
RUN apt-get update && apt-get install -y git build-essential cmake wget \
	pkg-config ca-certificates curl gnupg libpng16-16 libpng-dev \
	mesa-common-dev libglu1-mesa-dev libx11-6 xutils dbus xvfb

RUN apt-cache depends libqt5gui5|grep Depends:|sed 's/.*Depends:\s*\ //'|grep -v qt|xargs apt-get install -y

# Download the offline installer and run the installation
RUN wget https://download.qt.io/new_archive/qt/5.4/5.4.0/qt-opensource-linux-x64-5.4.0.run && chmod +x qt-opensource-linux-x64-5.4.0.run

RUN { xvfb-run ./qt-opensource-linux-x64-5.4.0.run --script script.qs --platform minimal --verbose | tee install.log || true; } \
    && grep -q "SHOW FINISHED PAGE" install.log && mv install.log /opt/qt

RUN rm qt-opensource-linux-x64-5.4.0.run script.qs
RUN apt-get clean
```

L'image est basée sur une Debian, à laquelle on vient ajouter différentes applications et binaires, notamment Qt5.4 !<br>

- construction de l'image "*test-qt*" avec une version pre-built de Qt5.4
```
docker build -f Dockerfile -t test-qt .
```

-  vérification de la présence de l'image sur la machine
```
docker images -a

# voici ce qu'on obtient
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
test-qt      latest    62a31c219116   10 days ago   3.63GB
```

- lancement d'un container 
```
docker run -v "$PWD:$PWD" -w "$PWD" --rm -it test-qt /bin/bash
```

Ceci positionne le répertoire du container au même niveau que le répertoire de lancement du container (print working directory).

- une fois que le conteneur est lancé, on se retrouve dans son bash

```
apt-get install -y x11-apps imagemagick

cd test
export Qt5_DIR=/opt/qt/5.4/gcc_64
# build analogclock executable
cmake . && make

xvfb-run -f ~/.Xauthority -n 0 ./analogclock &
xwd -display :0 -root -silent | convert xwd:- png:../screenshot.png
``` 