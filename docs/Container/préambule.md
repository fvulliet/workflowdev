# Conteneurisation

---
> *Ou comment s'affranchir de notre environnement de build, et le partager entre développeurs et entre machines.*

---

La conteneurisation consiste à rassembler le code d'une application ainsi tous ses composants (bibliothèques, frameworks et autres dépendances) de manière à les **isoler** dans leur propre « conteneur ». 

---
> Les conteneurs partagent le même noyau hôte mais sont isolés les uns des autres. 

---
	
Contrairement aux hyperviseurs qui fournissent beaucoup de resources hardware à chacune des machines virtuelles qu'ils gèrent (en créant de facto des overheads importants), les applications conteneurisées partagent un **OS commun** (celui de la machine hôte); elles sont compartimentées

- les unes des autres 
- et vis à vis du système

L'application "conteneurisée" peut ainsi être **déplacée** de manière monolithique et exécutée de façon **déterministe**, dans de multiples environnements / infrastructures, indépendamment de leur système d'exploitation !

---
> **En plaçant une application dans un conteneur, elle dispose de tout ce dont elle a besoin pour fonctionner (CPU, mémoire, resources réseau..), quel que soit l'endroit où elle est placée**. 

---

Enfin, au delà de faciliter la mise en place de nouvelles versions de logiciels, les conteneurs permettent aussi de revenir facilement à une version antérieure !
