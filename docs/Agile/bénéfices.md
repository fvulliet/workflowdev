# Bénéfices 

- Les équipes travaillent avec des **objectifs tangibles** et des **deadlines perceptibles**
    - *« étudions et maîtrisons d’abord le volant avant de nous lancer sur la fabrication de la voiture »*
- Les commanditaires bénéficient rapidement et régulièrement des **ajouts de valeur** qu'ils ont demandés et priorisés; ça permet de confirmer / infirmer / affiner le besoin 

![feedback loop](../../../img/agile_transfer.png "feedback loop")

- Les commanditaires sont servis **au bon moment** avec un logiciel opérationnel et **testé**
- Les opportunités et/ou aléas sont facilement appréhendés, puisque chaque fin de sprint est l'occasion d'une **remise en question**