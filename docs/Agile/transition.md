# Comment engager la transition 

## Mobiliser les équipes
La transition vers les méthodes Agiles est l'affaire de tous.

- Embarquer l'ensemble des communautés concernées (dev, projet, système, mktg)
    - qui ? les **managers**
- Pour chaque projet candidat, définir et prioriser les besoins
    - qui ? le **Product Owner**
- S'assurer de la disponibilité et de l'adhésion de tous pour suivre le rythme des sprints
    - qui ? le **Scrum Master**
- Identifier les besoins hardware pour les levées de risques et les évaluations techniques
    - qui ? les **chef(s) de projet**

## Changer de paradigme
- Accepter collectivement la réduction du scope afin de réussir les incréments de valeur
    - Passer de *« je veux tout dans 1 an »* à *« je veux quelques éléments dans 1 semaine »*  
