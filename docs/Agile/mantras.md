# Quelques "mantras" à retenir

- L'Agilité est un ensemble de **valeurs et de principes**, ce n'est pas un outil ou un framework
- L'Agilité n’est pas un objectif mais un **moyen** d'atteindre des objectifs
- Agile propose une approche **holistique** plutôt qu’une succession d’étapes : 
    - « rugby » plutôt que course en relais (« cycle en V »)
- Être Agile, c'est se demander en permanence: comment puis-je aider les membres de mon équipe pour apporter toujours plus de **valeur** au projet ?
- Être Agile c'est agir sur le **scope** autant que sur le coût ou le délai : 
    - " juste assez, juste à temps ! »
- Être Agile c'est **accepter d'échouer/d'évoluer au plus tôt** afin d'apprendre/corriger au plus tôt : 
    - « fail fast, learn fast »