# Scrum 

![scrum1](../../../img/rugby_scrum.jpg "Scrum1")

Scrum est l'une des méthodes Agiles les plus populaires. <br>
Le développement des fonctionnalités décrites dans le backlog est découpé en **sprints**, courus collectivement.

![scrum2](../../../img/agile_process.png "Scrum2")

Plusieurs **rituels** donnent du rythme à l'équipe.

![scrum3](../../../img/scrum.png "Scrum3")