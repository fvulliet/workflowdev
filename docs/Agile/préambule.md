# Quelques mots sur l'Agilité 

Le *Workflow* tel qu'il a été présenté au préablable n'implique pas obligatoirement la mise en place de méthodes Agiles. <br>

---
> *Cependant, si on décide de suivre un processus Agile dans la gestion de nos projets, le recours à un Workflow, et notamment aux pipelines de CI/CD, semble incontournable !*

---

## Le manifeste Agile

2001: des spécialistes se réunissent pour tenter de répondre aux nouvelles spécificités du développement logiciel, notamment l'instabilité et l'évolutivité du besoin (=gestion des changements). <br>
Il en résulte un **Manifeste**; il décrit "l’état d’esprit agile" au travers de 4 valeurs et 12 principes sous-jacents.

---
- « Les **individus et leurs interaction**, plus que les processus et les outils »
- « Des **logiciels opérationnels**, plus qu’une documentation exhaustive »
- « La **collaboration avec les clients**, plus que la négociation contractuelle » 
- « L’**adaptation au changement**, plus que le suivi d’un plan » 

---
Ces valeurs ont pour objectif de favoriser un haut niveau de performance pour les équipes et de sécuriser la livraison rapide de valeurs aux utilisateurs.

### Attention aux pièges
Le Manifeste n'est pas un texte "sacré" à appliquer à la lettre. Il s'agit surtout de s'efforcer de respecter au mieux ces valeurs et principes. <br>
L'apparente simplicité de ces valeurs limite parfois la perception de leur haute importance dans toute démarche agile.

Le piège est de porter les efforts sur les outils et les process. 

---
> *Il ne suffit pas de coller des post-its au mur pour être agile !*

---

La réussite d'une transition agile passe par le fait de faire évoluer la conception que l'on a du travail, de ses relations avec les collègues, du droit à l'erreur, de l'autonomie, de la confiance... <br>
Le changement d'outil est clairement la partie "simple" de la transition. **Faire changer les mentalités et les habitudes est moins facile**.

## Vers une tentative de définition de l'agilité
---
>*Approche holistique permettant de concentrer les énergies d’une communauté autour d’un périmètre pertinent, identifié, et encadré*:

---
- **communauté**: notion d'équipe, de rôles, et importance de l'engagement de tous
- **périmètre**: backlog avec epics & user stories 
- **pertinent**: importance de la priorisation permanente dans le backlog
- **identifié**: notion de sprint
- **encadré**: rythme, donné par les différents cérémoniaux
