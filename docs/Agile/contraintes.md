# Quelques contraintes à intégrer 

- Maîtriser les **besoins** !
    - *le PO sait ce qu’il veut, il sait quand il le veut, il sait l’exprimer, et il sait mesurer que c'est atteint*
- Apprendre à se **synchroniser** avec les autres métiers (élec et méca)
    - *accepter de travailler sur des versions intermédiaires / POC (tout en s’efforçant de capitaliser)*
- Développer en favorisant « l’ouverture aux **évolutions** »
    - *les règles de conception (découpage en objets, encapsulation, librairies..) et le workflow doivent favoriser la capitalisation et l’évolutivité*
- Rester vigilant après la phase de faisabilité / évaluation. 
    - *au-delà, trop de nouveautés et les remises en questions peuvent introduire des risques !*