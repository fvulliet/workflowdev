# Pourquoi devenir agile ?

- Pour répondre aux attentes du client / commanditaire, en lui délivrant de la valeur 	

	- au moment où il en a vraiment besoin
	- le plus régulièrement possible

![MVP](../../../img/Inflectiv-MVP-scaled.jpg "MVP")

- Pour favoriser une **boucle de retour** vertueuse
	- « échouer vite pour apprendre vite »
- Pour accueillir les **opportunités** et faire face aux **aléas**, plus facilement
- Pour **responsabiliser** les équipes et maintenir leur niveau d’**engagement**
	- les rituels réguliers cadencent le travail
	- « je dis ce que je vais faire et je fais ce que j’ai dit »
	- « plus vite démarré, plus vite fini »
- Pour garantir un bon niveau de **livrabilité**
	- grâce au workflow et à l’intégration continue (CI/CD)