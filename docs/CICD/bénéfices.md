# Quelques bénéfices 

## pour les développeurs

- **Indépendance**: « l'usine de fabrication » est délocalisée des machines des développeurs
- **Feedback**: le développeur connait constamment l'état de santé de l'application 
- **Qualité/maintenabilité**: le développeur accède à des métriques qui l'aident à augmenter la qualité du code
- **Scripts**: les mécanismes de la CI/CD sont des scripts ou du code => archivable, maintenable

## pour les intégrateurs

- **Déterminisme**: une seule « usine de fabrication » qui garantit des conditions reproductibles
- **Partage**: l'usine est accessible, utilisable, maintenable par tous les acteurs accrédités
- **Bande passante**: l'usine travaille à la demande, 24/7 !

## pour les managers

- **Visibilité**: les artefacts de sortie (rapports de tests, métriques, doc, releases) sont déposés dans un référentiel partagé, et offrent de la visibilité aux parties prenantes (les managers, les CP, les PO, et les équipes)
