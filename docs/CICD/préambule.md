# Continuous Integration / Continuous Delivery

L'Intégration Continue consiste à intégrer chaque nouvel incrément de code dans le tronc commun, et de s'assurer du **maintien de la qualité** du code, ainsi que du bon fonctionnement du logiciel et du système dans son intégralité, le tout de façon **automatisée**, dans un environnement de travail **déterministe**, et que l'on sait (re)déployer à la demande.

---
> **In fine, il s'agit d'augmenter la qualité et réduire le temps nécessaire à la livraison des applications**

---

En substance, la mise en place d'une CICD permet de:

- **Déléguer** les tâches répétitives de build, de test et de déploiement
- **Automatiser** et appareiller les étapes de développement des applications 
- Signaler les problèmes au plus **tôt** et favoriser des **feedbacks** réguliers (mise en place d'une boucle vertueuse)

![cicd](../../../img/cicd_pipelines.png "cicd")

