# A quel prix ?

- Adhésion des managers
- Montée en compétence des équipes 
- Se doter des outils nécessaires; ils sont multiples, évolutifs, la veille technologique est recommandée
    - Solutions open source (gratuites ou presque)
    - Solutions « tout-intégré »
- Collaboration avec les équipes IT: mise à disposition, administration, et disponibilité des serveurs et/ou VMs (locaux ou distants)
- Fédérer l'ensemble des équipes dév/IT pour ne pas isoler 1 ou 2 personnes sur un rôle d'intégrateur / ingé infra

---
> "Note: la CI/CD est nécessaire dans une démarche Agile, pour garantir le rythme et garantir les incréments de valeur; mais la réciproque n’est pas obligatoire"

---
