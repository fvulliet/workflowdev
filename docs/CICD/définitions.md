# Quelques définitions

## Intégration continue

---
> *incrémentation automatique et permanente de la chaine de valeur d'un logiciel, au rythme des évolutions apportées par les développeurs dans un référentiel partagé*

---

La CI permet aux développeurs d'intégrer (fusionner) fréquemment leurs modifications de code. <br>
Elles sont alors validées grâce à la création automatique d'une application et l'exécution de différents niveaux de **tests automatisés** (TU/TI) qui permettent de **vérifier la non-régression**. <br>
L’état de santé de l’application est accessible en permanence. <br>
La boucle de feedback CI permet de résoudre les dysfonctionnements plus tôt, plus facilement, plus rapidement et plus fréquemment. 


## Distribution/Déploiement continu

---
> *mise à disposition automatique, déterministe, et régulière des livrables logiciels dans un référentiel partagé (dev ou prod)*

---

La distribution continue automatise la **publication** du code validé dans un référentiel « interne ». <br>
Elle permet de disposer d'un code toujours prêt à être déployé dans un environnement de production. <br>
Elle profite aux développeurs, testeurs, chefs de projet.

## Pipeline

---
> *enchainement séquentiel automatique et appareillé des étapes de la création des artefacts du développement logiciel (code, test, doc, livrables..)*

---
